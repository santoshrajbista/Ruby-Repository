class SplatOperator
  
  def addAsSplatOperator(*numbers)
    numbers.inject do |sum,number|
      sum + number
    end
  end
  
  def addAsNormal(a,b,c,d,e)
    a+b+c+d+e
  end
  
  def methodWithDefaultParameterValue(a,b,c,d,e = 10)
    a+b+c+d+e
  end
  
end

splatOperator = SplatOperator.new()

sumAsSplatOpeartor = splatOperator.addAsSplatOperator(1,2,3,4,5,6,7,8,9,10)
puts sumAsSplatOpeartor

sumAsNormalOpeartor = splatOperator.addAsNormal(5,4,3,2,1)
puts sumAsNormalOpeartor

sumAsDefaultOperator = splatOperator.methodWithDefaultParameterValue(5,4,3,2)
puts sumAsDefaultOperator
