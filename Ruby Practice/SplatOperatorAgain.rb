class SplatOperatorAgain
  
  def splatOperatorAsParameter(*numbers)
    numbers.inject do |sum, number| 
      sum + number
    end
  end
  
  
  def splatAsAnArgument(a,b,c,d,e)
    a+b+c+d+e
  end
  
  def splatAsPartnerParameter(message,*numbers)
    "#{message}#{splatOperatorAsParameter(*numbers)}"
  end
  
end

sampleArray = [1,2,3,4,5]
splatOperatorAgain = SplatOperatorAgain.new()

splatOperatorAsParameter = splatOperatorAgain.splatOperatorAsParameter(1,2,3,4,5)
puts splatOperatorAsParameter


splatOperatorAsArgumnet = splatOperatorAgain.splatOperatorAsParameter(*sampleArray)
puts splatOperatorAsArgumnet

splatAsPartnerParams = splatOperatorAgain.splatAsPartnerParameter("The sum is : ",*sampleArray)
puts splatAsPartnerParams

