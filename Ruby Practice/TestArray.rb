class TestArray
  sampleArray = Array.new()
  evenArray = Array.new()
  oddArray = Array.new()
  
  100.times do |x|
    sampleArray.push(x)
  end
  
  evenArray = sampleArray.select do |x|
    x.even? == true
  end
  
  oddArray = sampleArray.select do |x|
    x.even? == false
  end
  
  evenArray.each do |x|
    puts "#{x}"
  end
  
  oddArray.each do |x|
      puts "#{x}"
    end
  
  sampleArray = sampleArray.delete_if do |x|
    x.even? == true
  end
  
    sampleArray.each do |x|
        puts "#{x}"
      end
 
  for i in sampleArray
    puts "#{i}"
  end
end