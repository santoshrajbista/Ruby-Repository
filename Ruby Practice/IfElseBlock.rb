class IfElseBlock
  def check_number(number)
      if number > 0
        puts "#{number} is positive number"
      elsif number < 0
        puts "#{number} is negative number"
      else
        puts "#{number} is equal to zero"
      end
    end 
    
    def check_number(number)
      puts "#{ number > 0 ?"#{number} is positive number" :"#{number} is negative number"}"
    end 
end
  
practice = IfElseBlock.new()
practice.check_number(9)
practice.check_number(-9)
practice.check_number(0)
